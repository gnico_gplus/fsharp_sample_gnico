﻿// F# の詳細については、http://fsharp.net を参照してください
// 詳細については、'F# チュートリアル' プロジェクトを参照してください。
open System
open System.Windows.Forms
open System.IO
open System.Diagnostics

[<EntryPoint>]
[<STAThreadAttribute>]
let main argv = 
    try
        let mutable proc : Process = new Process()
        proc.StartInfo.FileName <- "python"
        proc.StartInfo.UseShellExecute <- false
        proc.StartInfo.RedirectStandardOutput <- true
        proc.StartInfo.RedirectStandardInput <- false
        proc.StartInfo.CreateNoWindow <- true
        proc.StartInfo.Arguments <- "--version"
        proc.Start() |> ignore
        let reslut : string = proc.StandardOutput.ReadToEnd()
        proc.WaitForExit()
        proc.Close()
        Console.WriteLine("Version: " + reslut.Substring(reslut.IndexOf(' ') + 1))
        if reslut.[reslut.IndexOf(' ') + 1] >= '3' then
            MessageBox.Show("You don't have the python 3","Error",MessageBoxButtons.OK,MessageBoxIcon.Error) |> ignore
            Environment.Exit(0)
    with
       | exc ->  MessageBox.Show("Found not pyhon","Error",MessageBoxButtons.OK,MessageBoxIcon.Error) |> ignore
                 Environment.Exit(0)
    printfn "Please Select \"2to3.py\""
    let osdig : OpenFileDialog = new OpenFileDialog()
    osdig.Filter <- "Python 2 to 3 File|2to3.py|all files|*.*"
    osdig.FilterIndex <- 1
    if osdig.ShowDialog() = DialogResult.Cancel then
        Environment.Exit(0)
        0
    else
        let mutable proc : Process = new Process()
        proc.StartInfo.FileName <- "python"
        proc.StartInfo.CreateNoWindow <- true
        let rec repeatMethod (count : int, (PythonFiles : string[], ( proc : Process, TwoToThree : string))) =
            if count = PythonFiles.Length then
                0
            else
                proc.StartInfo.Arguments <- TwoToThree + " -w " + PythonFiles.[count]
                proc.Start() |> ignore
                proc.WaitForExit()
                repeatMethod(count + 1, (PythonFiles, (proc, TwoToThree)))
        repeatMethod(0,(Directory.GetFiles(Environment.CurrentDirectory, "*.py", SearchOption.AllDirectories), (proc, osdig.FileName))) |> ignore
        MessageBox.Show("Completed Python 2 to 3","Completed",MessageBoxButtons.OK,MessageBoxIcon.Information) |> ignore
        0