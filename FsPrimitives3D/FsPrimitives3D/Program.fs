﻿namespace FsPrimitives3D
open System
open System.Collections.Generic
open System.Linq

module main =
              [<EntryPoint>]
              [<STAThreadAttribute>]
              let main (args : string[]) = use game = new Primitives3DGame() in game.Run(); 0