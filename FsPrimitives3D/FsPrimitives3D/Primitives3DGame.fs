﻿namespace FsPrimitives3D
open System
open System.Collections.Generic
open Microsoft.Xna.Framework
open Microsoft.Xna.Framework.Content
open Microsoft.Xna.Framework.Graphics
open Microsoft.Xna.Framework.Input
open Microsoft.Xna.Framework.Storage
open Microsoft.Xna.Framework.GamerServices
/// <summary>
/// このサンプルは、立方体、球、円柱などの 3D ジオメトリ プリミティブを
/// 描画する方法を示します。
/// </summary>
type Primitives3DGame () as this =
    inherit Game()
    let mutable graphics : GraphicsDeviceManager = new GraphicsDeviceManager(this)

    let mutable spriteBatch = lazy new SpriteBatch(this.GraphicsDevice)
    let mutable spriteFont = lazy this.Content.Load<SpriteFont>("hudFont")
    
    let primitives = new List<GeometricPrimitive>()
    do
        this.Content.RootDirectory <- "Content"

    /// <summary>
    /// Allows the game to perform any initialization it needs to before starting to run.
    /// This is where it can query for any required services and load any non-graphic
    /// related content.  Calling base.Initialize will enumerate through any components
    /// and initialize them as well.
    /// </summary>
    override this.Initialize() =
        /// TODO: 初期化コードの挿入
        base.Initialize()

    /// <summary>
    /// LoadContent will be called once per game and is the place to load
    /// all of your content.
    /// </summary>
    override this.LoadContent() =
        /// TODO: スプライトやテクスチャをココで作成
        base.LoadContent()

    /// <summary>
    /// UnloadContent will be called once per game and is the place to unload
    /// all content.
    /// </summary>
    override this.UnloadContent() =
        base.UnloadContent()
        /// TODO: ココで内容をアンロード

    /// <summary>
    /// Allows the game to run logic such as updating the world,
    /// checking for collisions, gathering input, and playing audio.
    /// </summary>
    /// <param name="gameTime">Provides a snapshot of timing values.</param>
    override this.Update(gameTime : GameTime) =
        if GamePad.GetState(PlayerIndex.One).Buttons.Back = ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape) then
            this.Exit()
        /// TODO: 状態の更新をココに書く
        base.Update gameTime

    /// <summary>
    /// This is called when the game should draw itself.
    /// </summary>
    /// <param name="gameTime">Provides a snapshot of timing values.</param>
    override this.Draw(gameTime : GameTime) =
        this.GraphicsDevice.Clear(Color.CornflowerBlue)
        // TODO: Add your drawing code here
        base.Draw(gameTime)