﻿namespace FsPrimitives3D
open Microsoft.Xna.Framework
open Microsoft.Xna.Framework.Graphics

/// <summary>
/// 位置と法線のみを持ち、テクスチャー座標を
/// 持たない頂点のカスタム頂点タイプ。
/// </summary>
type VertexPositionNormal (pos : Vector3, nor : Vector3 )=
    let mutable Position = pos
    let mutable Normal = nor
    static let vertexDeclaration = new VertexDeclaration(new  VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0), new VertexElement(12, VertexElementFormat.Vector3, VertexElementUsage.Normal, 0))
    
    interface IVertexType with
        member x.VertexDeclaration: VertexDeclaration = 
            vertexDeclaration