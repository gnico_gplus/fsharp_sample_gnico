﻿namespace FsPrimitives3D
open System
open System.Collections.Generic
open Microsoft.Xna.Framework
open Microsoft.Xna.Framework.Graphics
/// <summary>
/// 単純なジオメトリ プリミティブ モデルの基底クラス。これは、頂点バッファー、
/// インデックス バッファー、およびモデルを描画するためのメソッドを
/// 提供します。特定のタイプのプリミティブのクラス (CubePrimitive、
/// SpherePrimitive など) は、この共通の基底クラスから派生し、
/// AddVertex メソッドおよび AddIndex メソッドを使用して、そのジオメトリを
/// 指定します。
/// </summary>
type GeometricPrimitives () as MeGP =
    // プリミティブ モデルの構築のプロセス中に、頂点およびインデックスの
    // データは、CPU 上でこれらの管理対象のリスト内に格納されます。
    let mutable vertices = new List<VertexPositionNormal>()
    let mutable indices = new List<uint16>()

    // すべてのジオメトリが指定されると、InitializePrimitive メソッドが、
    // 頂点およびインデックスのデータをこれらのバッファーに
    // コピーし、バッファーが、これらのデータを、効率的なレンダリングの
    // ために準備が整えられた GPU に格納します。
    let mutable vertexBuffer : VertexBuffer = null;
    let mutable indexBuffer : IndexBuffer = null;
    let mutable basicEffect : BasicEffect = null;

    /// <summary>
    /// 現在の頂点のインデックスをクエリします。これは、0 から
    /// 開始し、AddVertex が呼び出されるたびにインクリメントします。
    /// </summary>
    let CurrentVertex : int = vertices.Count

    interface IDisposable with

        member this.Dispose() =
            MeGP.Dispose(true)
            GC.SuppressFinalize(this);
                
    member MeGP.Dispose(disposing : bool) =
            if disposing then
                if vertexBuffer != null then
                    vertexBuffer.Dispose()
                if indexBuffer != null then
                    indexBuffer.Dispose() 
                if basicEffect != null then
                    basicEffect.Dispose()

    /// <summary>
    /// 新しい頂点をプリミティブ モデルに追加します。これは、
    /// 初期化プロセス中に、InitializePrimitive の前に
    /// 呼び出す必要があります。
    /// </summary>
    member MeGP.AddVertex(position : Vector3, normal : Vector3) =
        vertices.Add(new VertexPositionNormal(position, normal))

    /// <summary>
    /// 新しいインデックスをプリミティブ モデルに追加します。これは、
    /// 初期化プロセス中に、InitializePrimitive の前に
    /// 呼び出す必要があります。
    /// </summary>
    member MeGP.AddIndex ( index : int ) =
        if uint16 index > System.UInt16.MaxValue then
            raise (new ArgumentOutOfRangeException("index"))
        indices.Add( uint16 index )

    /// <summary>
    /// AddVertex および AddIndex の呼び出しによって、すべてのジオメトリが
    /// 指定されると、このメソッドは、頂点およびインデックスのデータを、
    /// 効率的なレンダリングのために準備が
    /// 整えられた GPU フォーマット バッファーにコピーします。
    member MeGP.InitializePrimitive( graphicsDevice : GraphicsDevice ) =
        // 頂点データのフォーマットを示す頂点宣言を作成します。

        // 頂点バッファーを作成し、それに頂点データをコピーします。
        vertexBuffer <- new VertexBuffer(graphicsDevice,
                                        typeof<VertexPositionNormal>,
                                        vertices.Count, BufferUsage.None)

        vertexBuffer.SetData(vertices.ToArray())

        // インデックス バッファーを作成し、それにインデックス データを
        // コピーします。
        indexBuffer <- new IndexBuffer(graphicsDevice, typeof<uint16>,
                                        indices.Count, BufferUsage.None)

        indexBuffer.SetData(indices.ToArray())

        // プリミティブのレンダリングに使用される BasicEffect を作成します。
        basicEffect <- new BasicEffect(graphicsDevice)

        basicEffect.EnableDefaultLighting()   