﻿let rec fact (rv : double , n : int) = 
    if n <= 1 then rv else fact(rv * double n, n - 1)

let rec power (rv : double ,( n : int, i : int)) =
    if i > n then rv else power(rv, (n, i + 1))

let rec mysin (rv : double, n : int) =
    if n > 100 then rv else mysin(rv + (double (if n % 2 = 1 then -1 else 1) / fact(1.0, 2 * n + 1) * power(rv, (2 * n + 1, 0))), n + 1)

[<EntryPoint>]
let main argv = 
    let mutable x : double = 0.0
    while x <= 20.0 do
        System.Console.Write(mysin(x, 0).ToString() + ", ")
        x <- x + 0.0001
    0 // 整数の終了コードを返します